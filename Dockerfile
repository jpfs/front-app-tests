FROM node:6.10.2-slim

RUN apt-get update && \
    apt-get install -y bzip2 libfontconfig && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /myproject

ADD yarn.lock package.json ./

RUN yarn

ADD . .
