var conf = require('../../nightwatch.conf.js');

const URL = 'http://int.sensedi.com';

module.exports = {
  before: function(browser) {
    console.log('Setting up...');
    browser
      .windowSize('current', 1024, 768)
      .url(URL)
      .waitForElementVisible('body')
      .login('ingger', 'password');
  },

  after: function(browser) {
    console.log('Closing down...');
    browser.end();
  },

  beforeEach: function(browser) {
    browser
      .pause(2500)
      .useCss();
  },
  'Dashboard': function(browser) {
    browser.url(URL + '/#/dashboard')   // visit the url
      .waitForElementVisible('body'); // wait for the body to be rendered

    browser.assert.containsText('md-card-header .mat-card-title', 'Últimos expedientes visitados'.toUpperCase())
      .saveScreenshot(conf.imgpath(browser) + 'login-post.png');
  },
  'Expedientes': function(browser) {
    browser.url(URL + '/#/expedientes')
      .waitForElementVisible('body');

    browser.assert.containsText('md-card-header .mat-card-title', 'LISTADO DE EXPEDIENTES')
      .saveScreenshot(conf.imgpath(browser) + 'expedientes.png');
  },
  'Histórico sistema': function(browser) {
    browser.url(URL + '/#/historicos/sistema')
      .waitForElementVisible('body');

    browser.assert.containsText('md-card-header .mat-card-title', 'Histórico de sistemas'.toUpperCase())
      .saveScreenshot(conf.imgpath(browser) + 'hist-sistema.png');
  },
  'Histórico aplicacion': function(browser) {
    browser.url(URL + '/#/historicos/aplicacion')
      .waitForElementVisible('body');

    browser.assert.containsText('md-card-header .mat-card-title', 'Histórico de aplicación'.toUpperCase())
      .saveScreenshot(conf.imgpath(browser) + 'hist-applicacion.png');
  },
  'Comunicaciones': function(browser) {
    browser.url(URL + '/#/comunicacion/interna')
      .waitForElementVisible('body');

    browser.assert.containsText('md-card-header .mat-card-title', 'Comunicaciones'.toUpperCase())
      .saveScreenshot(conf.imgpath(browser) + 'hist-applicacion.png');
  },
};
