exports.command = function(username, password, callback) {
  var self = this;
  this
    .frame(null)
    .waitForElementPresent('input[ng-reflect-name=username]', 10000)
    .clearValue('input[ng-reflect-name=username]')
    .waitForElementPresent('input[ng-reflect-name=password]', 10000)
    .clearValue('input[ng-reflect-name=password]')
    .setValue('input[ng-reflect-name=username]', username)
    .setValue('input[ng-reflect-name=password]', password)
    .click('button[type=submit]', function(response) {
      this.assert.ok(typeof response === 'object', 'We got a response object.');
    })
    .waitForElementNotPresent('@form');

  if (typeof callback === 'function') {
    callback.call(self);
  }
  return this; // allows the command to be chained.
};
